import 'package:flutter/material.dart';
import 'package:minggu_4_latihan_widget/Tugas12/model/Chart_model.dart';
import 'package:minggu_4_latihan_widget/Tugas12/drawer_screen.dart';



class Telegram extends StatefulWidget {
  @override
  _TelegramState createState()=> _TelegramState();
}

class _TelegramState extends State<Telegram> {
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: const Text("Telegram"), 
        actions: const [
          Padding(
            padding: EdgeInsets.all(8),
            child: Icon(Icons.search),
            )
        ],
        ),
        drawer:  DrawerScreen(),
        body: ListView.separated(
          itemBuilder: (ctx, i) {
            return ListTile(
              leading: CircleAvatar(
                radius: 28,
                backgroundImage: NetworkImage(items[i].profileUrl.toString()),
              ),
              title: Text(items[i].name.toString(), style: TextStyle(fontWeight: FontWeight.bold),),
              subtitle: Text(items[i].message.toString()),
              trailing: Text(items[i].time.toString()),
            );
          },
          separatorBuilder: (ctx, i){
            return const Divider();
          }, itemCount: items.length),
          floatingActionButton: FloatingActionButton(
            child: const Icon(Icons.create, color: Colors.white),
            backgroundColor: Color(0xFF65a9e0),
            onPressed: (){},
          ),
    );
  }
}