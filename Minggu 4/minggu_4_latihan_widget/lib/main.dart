import 'package:flutter/material.dart';
import 'package:minggu_4_latihan_widget/Tugas12/telegram.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key:key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
     home: Telegram(),
       
    );
  }

  Widget tes1(){
    return Scaffold(
        appBar: AppBar(
          title: Text("E41201570 Andi Wicaksono Nugroho"),
          backgroundColor: Colors.lightGreen,   
        ),
        body: Container(
          alignment: Alignment.center,
          child: Column(
            children: [
              Text("Latihan Minggu 4", style: TextStyle(
                color: Colors.white,
                backgroundColor: Colors.red,
                fontSize: 24.0,
                fontStyle: FontStyle.italic,
                fontWeight: FontWeight.bold
              ),
              ),

              tes2(),
              btnraised(),
              btn(),
              txtfld(),
              img()
              ]),
        ),
        );
  }

  Widget tes2() {
    return Center(
      child: Text("ini teks", style: TextStyle(
            color: Colors.blue,
            backgroundColor: Colors.pink,
            fontSize: 20.0,
            fontStyle: FontStyle.italic,
            fontWeight: FontWeight.bold,)
            ),
    );
  }

  Widget btnraised(){
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Column(
            children: <Widget>[
              Icon(Icons.access_alarm),
              Text('Alarm')
              ],
          ),
          Column(
            children: <Widget>[
              Icon(Icons.phone),
              Text('Phone')
              ],
          ),
          Column(
            children: <Widget>[
              Icon(Icons.book),
              Text('Book')
              ],
          ),
        ],
      ),
    );
  }

  Widget btn(){
    return Column(
      children: <Widget>[
        RaisedButton(
          color: Colors.amber,
          child: Text("Raised Button"),
          onPressed: () {},
        ),
        MaterialButton(
          color: Colors.lime,
          child: Text("Material Button"),
          onPressed: () {},
        ),
        FlatButton(
          color: Colors.lightGreenAccent,
          child: Text("FlatButton Button"),
          onPressed: () {},
        ),
      ],
    );
  }

  Widget txtfld(){
    return Padding(padding: const EdgeInsets.all(8.0),
      child: Form(
        child: Column(
          children: <Widget>[
            TextFormField(
              decoration: InputDecoration(hintText: "Username"),
            ),
            TextFormField(
              obscureText: true,
              decoration: InputDecoration(hintText: "Password"),
            ),
            RaisedButton(
              child: Text("Login"),
              onPressed: () {},
            )
          ],
        )
      ),
    );
  }

  Widget img(){
    return Center(
      child: Image.asset("gamis.jpg"),
    );
  }

}


