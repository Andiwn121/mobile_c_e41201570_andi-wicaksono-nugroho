import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Belajar form flutter",
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  const BelajarForm({ Key? key }) : super(key: key);

  @override
  State<BelajarForm> createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  
  final _formKey = GlobalKey<FormState>();
  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Belajar Form Flutter"),
      ),
      body: Form(
        child: SingleChildScrollView(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            children: [
              //textfield(),
              TextFormField(
                decoration: new InputDecoration(
                  hintText: "contoh: Andi Wicaksono Nugroho",
                  labelText: "Nama lengkap",
                  icon: Icon(Icons.people),
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(5.0)
                  )
                ),
                validator: (value){
                  if (value!.isEmpty) {
                    return 'nama tidak boleh kosong';
                  }
                  return null;
                },
              ),
              Padding(padding: EdgeInsets.all(20.0),
              child: TextFormField(
                obscureText: true,
                decoration: new InputDecoration(
                  hintText: "Masukkan password anda",
                  labelText: "Password",
                  icon: Icon(Icons.password),
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(5.0)),
                ),
                validator: (value){
                  if (value!.isEmpty) {
                    return 'password tidak boleh kosong';
                  }
                  return null;
                  },
                ),
              ),
              CheckboxListTile(
                title: Text('Belajar Dasar Flut'),
                subtitle: Text('Dart, Widget, http'),
                value: nilaiCheckBox,
                activeColor: Colors.deepPurpleAccent,
                onChanged: (value){
                  setState(() {
                    nilaiCheckBox = value!;
                  });
                },
              ),
              SwitchListTile(
                title: Text('Backend Prog'),
                subtitle: Text('Dart, Nodejs, PHP, Java, dll'),
                value: nilaiSwitch,
                activeColor: Colors.deepOrangeAccent,
                onChanged: (value){
                  setState(() {
                    nilaiSwitch = value;
                  });
                },
              ),
              Slider(
                value: nilaiSlider,
                min: 0,
                max: 100,
                onChanged: (value){
                  setState(() {
                    nilaiSlider = value;
                  });
                },
                ),

              Container(
                margin: EdgeInsets.only(top: 15.0),
                child: RaisedButton(
                  padding: EdgeInsets.all(20.0),
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.blue,
                  onPressed: (){
                    if (_formKey.currentState!.validate()) {}
                  },
                ),
              ),
            ],
          ),
        ),
      ),
      ),
    );
  }
}