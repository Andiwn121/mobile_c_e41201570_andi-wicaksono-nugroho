import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class RegistrasiForm extends StatefulWidget {
  const RegistrasiForm({ Key? key }) : super(key: key);

  @override
  State<RegistrasiForm> createState() => _RegistrasiFormState();
}

class _RegistrasiFormState extends State<RegistrasiForm> {

  List<String> pendidikan = [
    "SD/MI",
    "SMP/MTS",
    "SMA/SMK/Sederajat",
    "S1"
  ];

  final formKey = GlobalKey<FormState>();

  

  String _pendidikan = "SD/MI";
  String _kelamin = "";

  TextEditingController controllerNama = new TextEditingController();
  TextEditingController controllerTgl = new TextEditingController();
  TextEditingController controllerTelp = new TextEditingController();
  TextEditingController controllerMail = new TextEditingController();
  TextEditingController controllerPass = new TextEditingController();

  void _pilihKelamin(String value){
    setState(() {
      _kelamin = value;
    });
  }

  void pilihPendidikan(String value){
    setState(() {
      _pendidikan = value;
    });
  }

  void kirimdata(){
    AlertDialog alertDialog = new AlertDialog(
      content: new Container(
        height: 200.0,
        child: new Column(
          children: <Widget>[
            new Text("Nama Lengkap : ${controllerNama.text}"),
            new Text("Tanggal Lahir : ${controllerTgl.text}"),
            new Text("Jenis Kelamin : ${_kelamin}"),
            new Text("Jenjang Pendidikan : ${_pendidikan}"),
            new Text("No Telp : ${controllerTelp.text}"),
            new Text("E-mail : ${controllerMail.text}"),
            new Text("Password : ${controllerPass.text}"),
            new RaisedButton(
              child: new Text("OK"),
              onPressed: () => Navigator.pop(context),
              color: Colors.teal,
            ),
          ],
        ),
      )
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Registrasi"),
        backgroundColor: Colors.green[900],
      ),
      body: Form(
        key: formKey,
        child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              child: Padding(padding: const EdgeInsets.all(50.0),
                child: new Hero(
                 tag: "logo",
                 child: ClipOval(
                  child: SizedBox(
                    width: 130.0,
                    height: 130.0,
                    child: Image.asset("logo3.jpg", fit: BoxFit.cover,),
                  ),  
                 ),
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(left: 50.0, right: 50.0, top: 20.0),
              child: TextFormField(
                controller: controllerNama,
                decoration: new InputDecoration(
                  hintText: "Masukkan Nama Anda",
                  labelText: "Nama Lengkap",
                  icon: Icon(Icons.people),
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(15.0)),
                ),
                 validator: (value){
                  if (value!.isEmpty) {
                    return 'nama tidak boleh kosong';
                  }
                  return null;
                },
                ),
              ),
              Padding(padding: EdgeInsets.only(left: 50.0, right: 50.0, top: 20.0),
              child: TextFormField(
                keyboardType: TextInputType.datetime,
                controller: controllerTgl,
                decoration: new InputDecoration(
                  hintText: "Masukkan Tanggal Lahir",
                  labelText: "Tanggal Lahir",
                  icon: Icon(Icons.date_range),
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(15.0)),
                ),
                 validator: (value){
                  if (value!.isEmpty) {
                    return 'tgl tidak boleh kosong';
                  }
                  return null;
                },
                ),
              ),
              Padding(padding: new EdgeInsets.only(left: 50.0, right: 50.0, top: 20.0),
              child: Text("Jenis Kelamin :", 
              style: TextStyle(fontSize: 18), 
              ),
              ),
              Padding(padding: new EdgeInsets.only(left: 50.0, right: 50.0, top: 20.0),
              child: RadioListTile(
                value: "Laki - Laki", 
                title: new Text("laki - laki"),
                groupValue: _kelamin, 
                onChanged: (String? value){
                _pilihKelamin(value!);
              },
              activeColor: Colors.blue,
              subtitle: new Text("Pilih ini jika anda laki - laki"),
              ),
              ),
              Padding(padding: new EdgeInsets.only(left: 50.0, right: 50.0, top: 20.0),
              child: RadioListTile(
                value: "Perempuan", 
                title: new Text("Perempuan"),
                groupValue: _kelamin, 
                onChanged: (String? value){
                _pilihKelamin(value!);
              },
              activeColor: Colors.blue,
              subtitle: new Text("Pilih ini jika anda laki - laki"),
              ),
              ),
                Padding(
                  padding: const EdgeInsets.only(left: 50.0, right: 50.0, top: 20.0),
                  child: Row(
                    children: [
                      Text(
                        "Jenjang Pendidikan ", 
                        style: new TextStyle(fontSize: 18.0, color: Colors.blue),
                      ),
                      DropdownButton(
                        onChanged: (String? value){
                          pilihPendidikan(value!);
                        },
                        value: _pendidikan,
                        items: pendidikan.map((String value) {
                          return new DropdownMenuItem(
                            value: value,
                            child: new Text(value),
                          );
                        }).toList(),
                        ),
                    ],
                  ),
                ),
              Padding(padding: EdgeInsets.only(left: 50.0, right: 50.0, top: 20.0),
              child: TextFormField(
                keyboardType: TextInputType.phone,
                controller: controllerTelp,
                decoration: new InputDecoration(
                  hintText: "contoh: 0812xxxxxxxx",
                  labelText: "No. Telp",
                  icon: Icon(Icons.phone),
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(15.0)),
                ),
                 validator: (value){
                  if (value!.isEmpty) {
                    return 'telp tidak boleh kosong';
                  }
                  return null;
                },
                ),
              ),
              Padding(padding: EdgeInsets.only(left: 50.0, right: 50.0, top: 20.0),
              child: TextFormField(
                keyboardType: TextInputType.emailAddress,
                controller: controllerMail,
                decoration: new InputDecoration(
                  hintText: "Masukkan Email anda",
                  labelText: "E-mail",
                  icon: Icon(Icons.email),
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(15.0)),
                ),
                 validator: (value){
                  if (value!.isEmpty) {
                    return 'mail tidak boleh kosong';
                  }
                  return null;
                },
                ),
              ),
              Padding(padding: EdgeInsets.only(left: 50.0, right: 50.0, top: 20.0),
              child: TextFormField(
                obscureText: true,
                controller: controllerPass,
                decoration: new InputDecoration(
                  hintText: "Masukkan password anda",
                  labelText: "Password",
                  icon: Icon(Icons.key),
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(15.0)),
               ),
               validator: (value){
                  if (value!.isEmpty) {
                    return 'password tidak boleh kosong';
                  }
                  return null;
                  },
                ),
              ),
            Padding(
              padding: const EdgeInsets.all(50.0),
              child: Center(
                child: new RaisedButton(
                  
                  color: Colors.blue,
                  child: new Text(
                    "Daftar",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white),),
                  onPressed: (){
                    if (formKey.currentState!.validate()) {
                      kirimdata();
                    }
                  }),
              ),
            )
          ],
        ),
      ),
      ),
    );
  }
}