import 'package:flutter/material.dart';
import 'package:minggu_7/form.dart';
import 'package:minggu_7/form_register.dart';

void main() {
  runApp(new MaterialApp(
    debugShowCheckedModeBanner: false,
    home: new RegistrasiForm(),
  ));
}

class Home extends StatefulWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  
  List<String> agama = [
    "Islam",
    "Kristen Protestan",
    "Kristen Katolik",
    "Hindu",
    "Buddha"
  ];

  String _agama = "Islam";
  String _jk = "";

  TextEditingController controllerNama = new TextEditingController();
  TextEditingController controllerPass = new TextEditingController();
  TextEditingController controllerMoto = new TextEditingController();

  void _pilihJk(String value){
    setState(() {
      _jk = value;
    });
  }

  void pilihAgama(String value){
    setState(() {
      _agama = value;
    });
  }

  void kirimdata(){
    AlertDialog alertDialog = new AlertDialog(
      content: new Container(
        height: 200.0,
        child: new Column(
          children: <Widget>[
            new Text("Nama Lengkap : ${controllerNama.text}"),
            new Text("Password : ${controllerPass.text}"),         
            new Text("Moto Hidup : ${controllerMoto.text}"),   
            new Text("Jenis Kelamin : ${_jk}"),
            new Text("Agama : ${_agama}"),
            new RaisedButton(
              child: new Text("OK"),
              onPressed: () => Navigator.pop(context),
              color: Colors.teal,
            ),
          ],
        ),
      )
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        leading: new Icon(Icons.list),
        title: new Text("Data diri"),
        backgroundColor: Colors.teal,
      ),
      body: new Container(
        padding: new EdgeInsets.all(10.0),
        child: new Column(
          children: <Widget>[
            new TextField(
              controller: controllerNama,
              decoration: new InputDecoration(
                  hintText: "Nama lengkap",
                  labelText: "Nama lengkap",
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(20.0)
                  )
                ),
            ),
            new Padding(
              padding: new EdgeInsets.only(top: 20.0)
              ),
            new TextField(
              controller: controllerPass,
              obscureText: true,
              decoration: new InputDecoration(
                hintText: "Password",
                labelText: "Password",
                border: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(20.0)
                )
              ),
            ),
            new Padding(
              padding: new EdgeInsets.only(top: 20.0)
              ),
            new TextField(
              controller: controllerMoto,
              maxLines: 3,
              decoration: new InputDecoration(
                hintText: "Moto hidup",
                labelText: "Moto hidup",
                border: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(20.0)
                )
              ),
            ),
            new Padding(
              padding: new EdgeInsets.only(top: 20.0)
              ),
            new RadioListTile(
              value: "laki - laki", 
              title: new Text("laki - laki"),
              groupValue: _jk, 
              onChanged: (String? value){
                _pilihJk(value!);
              },
              activeColor: Colors.blue,
              subtitle: new Text("Pilih ini jika anda laki - laki"),
              ),
            new RadioListTile(
              value: "perempuan", 
              title: new Text("perempuan"),
              groupValue: _jk, 
              onChanged: (String? value){
                _pilihJk(value!);
              },
              activeColor: Colors.blue,
              subtitle: new Text("Pilih ini jika anda perempuan"),
              ),
            new Padding(
              padding: new EdgeInsets.only(top: 20.0),
            ),
            new Row(
              children: <Widget>[
                new Text(
                  "Agama ", 
                  style: new TextStyle(fontSize: 18.0, color: Colors.blue),
                ),
                new Padding(
                  padding: new EdgeInsets.only(left: 15.0), 
                ),
                DropdownButton(
                  onChanged: (String? value){
                    pilihAgama(value!);
                  },
                  value: _agama,
                  items: agama.map((String value) {
                    return new DropdownMenuItem(
                      value: value,
                      child: new Text(value),
                    );
                  }).toList(),
                  )
              ],
            ),
            new RaisedButton(
              child: new Text("OK"),
              color: Colors.blue,
              onPressed: (){
                kirimdata();
              })
          ],
          ),
      ),
    );
  }
}


