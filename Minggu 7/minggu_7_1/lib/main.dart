import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

 
  @override
  Widget build(BuildContext context) {
    
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
       
        primarySwatch: Colors.blue,
      ),
      home: widgetTextFF(),
    );
  }
}

class widgetTextFF extends StatelessWidget {
  const widgetTextFF({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Belajar Text Form"),
        ),
      body: Container(
        margin: EdgeInsets.all(20.0),
        child: Column(
          children: [
            TextFormField(
              autofocus: true,
              decoration: new InputDecoration(
                hintText: "Masukkan nama lengkap anda",
                labelText: "Nama lengkap",
                icon: Icon(Icons.people),
                border: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(5.0)),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20.0),
              child: TextField(
                obscureText: true,
                decoration: new InputDecoration(
                  hintText: "Password anda",
                  labelText: "Password",
                  icon: Icon(Icons.password),
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(5.0)),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20.0),
              child: TextFormField(
                keyboardType: TextInputType.phone,
                decoration: new InputDecoration(
                  hintText: "contoh: 0812xxxxxxxx",
                  labelText: "No. Telp",
                  icon: Icon(Icons.phone),
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(5.0)),
                ),
              ),
            )
          ],
          
        ),
      ),
      
    );
  }
}