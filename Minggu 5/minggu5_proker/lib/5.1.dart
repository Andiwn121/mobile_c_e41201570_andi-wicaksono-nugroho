import 'package:flutter/material.dart';

class widgettest extends StatelessWidget {
  const widgettest ({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.white,
          child:
          Text('+', 
          style: TextStyle(color: Colors.orange[900], fontSize: 24),), 
          onPressed: () {},
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(8.0), //mencoba margin
              width: 50, 
              height: 50, 
              decoration: BoxDecoration(
                color: Colors.redAccent,
                shape: BoxShape.circle),),
            Container(
              padding: EdgeInsets.all(8.0), //mencoba padding
              width: 50, 
              height: 50, 
              decoration: BoxDecoration(
                color: Colors.pinkAccent,
                shape: BoxShape.circle),),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  width: 50, 
                  height: 50, 
                  decoration: BoxDecoration(
                    color:Colors.blueAccent, 
                    shape: BoxShape.circle),),
                Container(
                  width: 50, 
                  height: 50, 
                  decoration: BoxDecoration(
                    color: Colors.redAccent,
                    shape: BoxShape.circle),),
                Container(
                  width: 50, 
                  height: 50, 
                  decoration: BoxDecoration(
                    color:Colors.pinkAccent, 
                    shape: BoxShape.circle),),
                ],
              )
            ],
          ),
      );
  }
}
