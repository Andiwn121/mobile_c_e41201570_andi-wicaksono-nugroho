import 'package:flutter/material.dart';
import 'package:minggu5_proker/5.1.dart';
import 'package:minggu5_proker/5.2.dart';
import 'package:minggu5_proker/nav/botnav.dart';



void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key:key);

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      debugShowCheckedModeBanner: false,
       home: BottomNav(),
       routes: {
         "/widgettest": (BuildContext context) => const widgettest(),
         "/widgetbtn": (BuildContext context) => const widgetbtn(),
       },
    );
  }
}