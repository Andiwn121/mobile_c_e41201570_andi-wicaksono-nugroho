import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:minggu5_proker/5.1.dart';

class DrawerScreen extends StatefulWidget {
  const DrawerScreen({Key? key}) : super(key: key);

  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context){
    return Drawer(
      child: ListView(
        children: <Widget>[
          const UserAccountsDrawerHeader(
            accountName: Text("Andi Wicaksono N"),
            currentAccountPicture: CircleAvatar(
              backgroundImage: AssetImage("andi.jpg"),
            ),
            accountEmail: Text("and@gmail.com"),
            ),
            DrawerListTile(
              iconData: Icons.home,
              title: "Home",
              onTilePressed: (){
                Navigator.pushNamed(context, "/widgettest");
              },
            ),
            DrawerListTile(
              iconData: Icons.search,
              title: "Search",
              onTilePressed: (){
                Navigator.pushNamed(context, "/widgetbtn");
              },
            ),
            DrawerListTile(
              iconData: Icons.account_box,
              title: "Akun",
              onTilePressed: (){},
            ),
        ]
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTilePressed;

  const DrawerListTile({Key? key, required this.iconData, required this.title, required this.onTilePressed})
  :super(key: key);
  @override
  Widget build(BuildContext context){
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(title, style: const TextStyle(fontSize: 16),),
    );
  }
}