import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class widgetbtn extends StatelessWidget {
  const widgetbtn({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: TextButton(
          onPressed:() {
          Navigator.pop(context);
        }, 
        child: Text("Coba", style: TextStyle(color: Colors.white)),
        style: TextButton.styleFrom(
          backgroundColor: Colors.amber,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15))
        ),
        ),
        
      ),
      appBar: AppBar(
        
        title: Text("Button"),
        
      ),
    );
  }
}