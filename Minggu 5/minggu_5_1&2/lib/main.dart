import 'package:flutter/material.dart';
void main() => runApp(MyApp());
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Belajar Routing'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: (){
            Route route = MaterialPageRoute(builder: (context) => AboutPage());
            Navigator.push(context, route);
          },
          child: Text('tap untuk ke AboutPage'),
          ),
      ),
    );
  }
}

class AboutPage extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tentang Aplikasi'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('Kembali'),
        )),
    );
  }
}


Widget tes1(){
  return Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.menu),
          title: Text("Shopee"),
          actions: <Widget>[
            Icon(Icons.search),
            // Icon(Icons.find_in_page)
          ],
          actionsIconTheme: IconThemeData(color: Colors.white),
          backgroundColor: Colors.orange[900],
          bottom: PreferredSize(
            child: Container(
              color: Colors.orange[200],
              height: 4.0,
            ),
            preferredSize: Size.fromHeight(4.0)
          ),
          centerTitle: true,
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.white,
          child:
          Text('+', 
          style: TextStyle(color: Colors.orange[900], fontSize: 24),), 
          onPressed: () {},
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(8.0), //mencoba margin
              width: 50, 
              height: 50, 
              decoration: BoxDecoration(
                color: Colors.redAccent,
                shape: BoxShape.circle),),
            Container(
              padding: EdgeInsets.all(8.0), //mencoba padding
              width: 50, 
              height: 50, 
              decoration: BoxDecoration(
                color: Colors.pinkAccent,
                shape: BoxShape.circle),),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  width: 50, 
                  height: 50, 
                  decoration: BoxDecoration(
                    color:Colors.blueAccent, 
                    shape: BoxShape.circle),),
                Container(
                  width: 50, 
                  height: 50, 
                  decoration: BoxDecoration(
                    color: Colors.redAccent,
                    shape: BoxShape.circle),),
                Container(
                  width: 50, 
                  height: 50, 
                  decoration: BoxDecoration(
                    color:Colors.pinkAccent, 
                    shape: BoxShape.circle),),
                ],
              )
            ],
          ),
      );
}

