import 'lingkaran.dart';

void main(List<String> args) {
  Lingkaran lk; // inisialisasi lingkaran
  double luasLingkaran; // inisialisasi tipe data luas lingkaran

  lk = new Lingkaran(); //menginisialisasi atau mengaliskan lk sebagai lingkaran/ pointer menunjuk object lingkaran

  luasLingkaran = lk.hitungLuas(); // allias luaslingkaran
  print(luasLingkaran); // mencetak luas lingkaran

}