class Lingkaran {
  double jari2 = -7; //initialisasi tipe data jari2
  double phi = 3.14; //initialisasi tipe data phi
  
  void setjari2(double nilai){
     if(nilai < 0){ // validasi jika nilai di bawah 0 
       nilai *= -1; // akan di kalikan -1, misal -1 * -2 hasilnya 2
     }
     jari2= nilai; //alias 
  }
  double getJari2(){ //get jari2
    return jari2; // mengembalikan nilai get jari2
  }
  
  double getPhi(){ //get panjang
    return phi; // mengembalikan nilai get phi
  }
  double hitungLuas(){
      return this. jari2 * jari2 * phi; //mereturn hasil
  }  
}