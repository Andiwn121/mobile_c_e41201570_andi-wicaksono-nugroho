import 'package:flutter/material.dart';

class WidgetGra extends StatelessWidget {
  const WidgetGra({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Flexible(
            flex: 1,
            child: Container(
              decoration: new BoxDecoration(
                gradient: new LinearGradient(
                  begin: Alignment.bottomLeft,
                  end: Alignment.topRight,
                  colors: [
                    Colors.amber,
                    Colors.pink,
                    Colors.purple,
                  ]
                )
              ),  
            ),
          ),
          Flexible(
            flex: 2,
            child: Container(
              decoration: new BoxDecoration(
                gradient: new RadialGradient(
                  radius: 1,
                  colors: [
                    Colors.amber,
                    Colors.pink,
                    Colors.purple,
                  ]
                )
              ),  
            ),
          ),
          Flexible(
            flex: 1,
            child: Container(
              decoration: new BoxDecoration(
                gradient: new SweepGradient(
                  startAngle: 0.1,
                  endAngle: 1.0,
                  colors: [
                    Colors.amber,
                    Colors.purple,
                  ]
                )
              ),  
            ),
          ),
        ],
      ),
    );
  }
}