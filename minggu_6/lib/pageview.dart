import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

class WidgetPV extends StatefulWidget {
  const WidgetPV({ Key? key }) : super(key: key);

  @override
  State<WidgetPV> createState() => _WidgetPVState();
}

class _WidgetPVState extends State<WidgetPV> {

  final List<String> gambar = [
    "andi.jpg",
    "poi.jpg",
    "bambang.png",
    "mega.jpg",
    "heri.jpg",
    "hilda.jpg",
    "anis.jpg",
    "nabila.jpg",
  ];

  static const Map<String, Color> colors = {
    'andi' : Color(0xFF2DB569),
    'poi' : Color(0xFFF386B8),
    'bambang' : Color(0XFF45CAF5),
    'mega' : Color(0xFFB19ECB),
    'heri' : Color(0XFF46C1BE),
    'hilda' : Color(0XFFF58E4C),
    'anis' : Color(0XFFFFEA0E),
    'nabila' : Color(0XFFDBE4E9),

  };

  @override
  Widget build(BuildContext context) {
    timeDilation = 5.0; 
    return Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
          gradient: new LinearGradient(
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
            colors: [
              Colors.amber,
              Colors.pink,
              Colors.purple,
            ]
          )
        ),
        child: new PageView.builder(
          controller: new PageController(viewportFraction: 0.8),
          itemCount: gambar.length,
          itemBuilder: (BuildContext context, int i){
            return new Padding(
              padding: 
                new EdgeInsets.symmetric(horizontal: 5.0, vertical: 50.0),
              child: new Material(
                elevation: 8.0,
                child: new Stack(
                 fit: StackFit.expand,
                 children: <Widget>[
                   new Hero(
                     tag: gambar[1], 
                     child: new Material(
                       child: InkWell(
                         child: new Flexible(
                           flex: 1,
                           child: Container(
                             color: colors.values.elementAt(i),
                             child: new Image.asset(
                               "/${gambar[i]}",
                               fit: BoxFit.cover,
                             ),
                           ),
                         ),
                         onTap: ()=> Navigator.of(context).push(
                           new MaterialPageRoute(
                             builder: (BuildContext context) => 
                                new Halamandua(
                                  gambar: gambar[i],
                                  colors: colors.values.elementAt(i),
                                ))),
                         ),
                       ))
                 ], 
                ),
              ));
          }),
      ),
    );
  }
}

class Halamandua extends StatelessWidget {
  const Halamandua({ Key? key, required this.gambar, required this.colors }) : super(key: key);

  final String gambar;

  final Color colors;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text("ANJAS"),
        backgroundColor: Colors.purpleAccent,
      ),
      body: new Stack(
        children: <Widget>[
          new Container(
            decoration: new BoxDecoration(
              gradient: new RadialGradient(
                center: Alignment.center,
                colors: [
                  Colors.purple,
                  Colors.white,
                  Colors.deepPurple
            ]),
          ),
         ),
         new Center(
           child: new Hero(
             tag: gambar, 
             child: new ClipOval(
               child: new SizedBox(
                 width: 200.0,
                 height: 200.0,
                 child: new Material(
                   child: InkWell(
                     onTap: () => Navigator.of(context).pop(),
                     child: new Flexible(
                       flex: 1,
                       child: Container(
                         color: colors,
                         child: new Image.asset(
                           "/$gambar",
                           fit: BoxFit.cover,
                         ),
                       )),
                   ),
                 )
               ),
             )),
         )
        ],
      )
    );
  }
}